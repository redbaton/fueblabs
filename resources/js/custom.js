$("#navigation").load("common/header.html");
$("footer").load("common/footer.html");


// Swiper function
$(document).ready(function() {
    $(window).scroll(function() {
        if ($(window).scrollTop() > 56) {
            $(".navbar").addClass("active");
        } else {
            $(".navbar").removeClass("active");
        }
    });
    // If Mobile, add background color when toggler is clicked
    $(".navbar-toggler").click(function() {
        if (!$(".navbar-collapse").hasClass("show")) {
            $(".navbar").addClass("active");
        } else {
            if ($(window).scrollTop() < 56) {
                $(".navbar").removeClass("active");
            } else {}
        }
    });
    $(document).scroll(function(event) {

        var clickover = $(event.target);
        var _opened = $(".navbar-collapse").hasClass("show");
        if (_opened === true && !clickover.hasClass("navbar-toggler")) {
            $(".navbar-toggler").click();
            $('.navbar').removeClass("active");
        }

    });

    function myFunction(x) {
        x.classList.toggle("change");
    }

    // var myFullpage = new fullpage('#smartCity', {
    //     anchors: ['1', '2', '3', '4', '5', '6', '7'],
    //     // sectionsColor: ['#4A6FB1', '#939FAA', '#323539'],
    //     verticalCentered: true,
    //     scrollBar: true,
    //     scrollingSpeed: 700,
    //     loopTop: false,
    //     keyboardScrolling: false,
    //     autoScrolling:false,
    //     // scrolloOverflow:true,
    //     // scrollOverflow: true,
    //      // autoScrolling:false,
    // });


    /*......hover dropdown menu function.....*/
    $('.navbar .dropdown').hover(function() {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(150).slideDown();
    }, function() {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp()
    });
    /*........end...........*/

    var swiper = new Swiper('#emply-testimonial .swiper-container', {
        slidesPerView: 2,
        spaceBetween: 50,
        pagination: {
            el: '#emply-testimonial .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '#emply-testimonial .swiper-button-next',
            prevEl: '#emply-testimonial .swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 0,
            }

        }
    });
    var swiper = new Swiper('#vertical .swiper-container', {
        slidesPerView: 2,
        spaceBetween: 50,
        pagination: {
            el: '#vertical .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '#vertical .swiper-button-next',
            prevEl: '#vertical .swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 0,
            }

        }
    });
    var swiper = new Swiper('#team-slider .swiper-container', {
        slidesPerView: 2,
        spaceBetween: 50,
        pagination: {
            el: '#team-slider .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '#team-slider .swiper-button-next',
            prevEl: '#team-slider .swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 0,
            }

        }
    });

    var swiper = new Swiper('#banner-slider .swiper-container', {
        direction: 'vertical',
        mousewheel: false,
        slidesPerView: 1,
        allowTouchMove: false,
        simulateTouch: false,
        spaceBetween: 0,

        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            type: 'fraction',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    var aboutImg = new Swiper('.about-img .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 0,
        centeredSlides: true,
        pagination: '.about-img .swiper-pagination',
        hashnav: true,
        hashNavigation: {
            watchState: true,
        }

    });

    var abouttxt = new Swiper('.about-txt .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 0,
        centeredSlides: true,
        hashnav: true,
        hashNavigation: {
            watchState: true,
        },
        navigation: {
            nextEl: '.about-txt .swiper-button-next',
            prevEl: '.about-txt .swiper-button-prev',
        }
    });
    var swiper = new Swiper('#case-studies .swiper-container', {
        slidesPerView: 3,
        spaceBetween: 40,
        // freeMode: true,
        pagination: {
            el: '#case-studies .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '#case-studies .swiper-button-next',
            prevEl: '#case-studies .swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 0,
            }

        }
    });
    var swiper = new Swiper('#testimonial .swiper-container', {
        slidesPerView: 3,
        cssWidthAndHeight: true,
        spaceBetween: 30,
        centeredSlides: true,
        loop: true,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '#testimonial .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '#testimonial .swiper-button-next',
            prevEl: '#testimonial .swiper-button-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 1,
                spaceBetween: 10,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10,
            }

        }
    });
    var swiper = new Swiper('#section3 .swiper-container', {
        slidesPerView: 5,
        spaceBetween: 30,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        // breakpoints: {
        //     768: {
        //         slidesPerView: 1,
        //         spaceBetween: 10,
        //     },
        //     640: {
        //         slidesPerView: 1,
        //         spaceBetween: 20,
        //     },
        //     320: {
        //         slidesPerView: 1,
        //         spaceBetween: 10,
        //     }

        // }
    });

    // blog
    var swiper = new Swiper('#blog-slider .swiper-container', {
        slidesPerView: 2,
        spaceBetween: 50,
        pagination: {
            el: '#blog-slider .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '#blog-slider .swiper-button-next',
            prevEl: '#blog-slider .swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 0,
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 0,
            }

        }
    });

    $('a.smooth-scroll[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

function initMap() {
    // The location of Uluru
    var currentlat = { lat: 12.8989455, lng: 77.6303789 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 18,
        center: currentlat
    });

    var marker = new google.maps.Marker({
        position: currentlat,
        map: map,
        title: 'CLUSTR(Tally Analytics Pvt Ltd.)'
    });
}